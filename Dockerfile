FROM node:14 AS ui-build
WORKDIR /usr/src/app
COPY phone-book-app/ ./phone-book-app/
RUN cd phone-book-app && npm install && npm run build

FROM node:14 AS server-build
WORKDIR /root/
COPY --from=ui-build /usr/src/app/phone-book-app/build ./phone-book-app/build
COPY backend/package*.json ./backend/
RUN cd backend && npm install
COPY backend/*.js ./backend/
COPY backend/routes/ ./backend/routes/
COPY backend/models/ ./backend/models/

EXPOSE 3000

CMD ["node", "./backend/app.js"]