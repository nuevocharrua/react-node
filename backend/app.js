const express = require('express');
const path = require('path');
const db = require('./database');
const config = require('./config');
const cors = require('cors')
var jwt = require('express-jwt');

const app = express();
app.use(express.json());
app.use(cors())

app.use(express.static(path.join(__dirname, '../phone-book-app/build')));

const port = config.web.port;
const jwtAuth = jwt({ secret: config.jwt.sectret, algorithms: [config.jwt.algorithm] });

var contacts = require('./routes/contacts');
var login = require('./routes/login');
var user = require('./routes/user');

app.use('/api/contacts', jwtAuth, contacts);
app.use('/api/login', login);
app.use('/api/user', user);
app.use('/api/*', (req, res) => {
    res.status(404).send('404 Not Found ')
});
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../phone-book-app/build/index.html'));
});
app.listen(port, () => {
    db.sync({ force: false }).then(() => {
        console.log('Connection has been established successfully.');
    }).catch((error) => {
        console.error('Unable to connect to the database:', error);
    });
    console.log(`Listening at http://localhost:${port}`)
})