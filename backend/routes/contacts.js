const express = require('express');
const Contact = require('../models/contact');

const router = express.Router();

router.get('/', function (req, res, next) {
    Contact.findAll({ where: { userId: req.user.userId }, order: [['firstName', 'ASC']] }).then(all => res.send(all));
});

router.get('/:id', function (req, res, next) {
    Contact.findOne({ where: { id: req.params.id, userId: req.user.userId } }).then(one => {
        if (one) {
            res.send(one);
        } else {
            res.status(404).send('404 Not Found');
        }
    });
});

router.post('/', function (req, res, next) {
    Contact.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phone: req.body.phone,
        userId: req.user.userId
    }).then(contact => {
        res.status(202).send(contact);
    });
});

router.delete('/:id', function (req, res, next) {
    Contact.destroy({ where: { userId: req.user.userId, id: req.params.id } }).then(one => {
        res.status(one ? 204 : 404).send(one ? '' : '404 Not Found');
    });
});

router.use('*', (req, res) => {
    res.status(405).send('405 Method Not Allowed');
});

module.exports = router;


