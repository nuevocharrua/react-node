const express = require('express');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('../config');

const md5 = require('md5');
const router = express.Router();

router.post('/', function (req, res, next) {
    User.findOne({ where: { username: req.body.username, password: md5(req.body.password) } }).then(user => {
        if (user) {
            var token = jwt.sign({ userId: user.id }, config.jwt.sectret, { algorithm: config.jwt.algorithm });
            res.send(token);
        } else {
            res.status(401).send();
        }
    })
});
router.use('*', (req, res) => {
    res.status(405).send('405 Method Not Allowed');
});

module.exports = router;


