const express = require('express');
const User = require('../models/user');
const md5 = require('md5');
const router = express.Router();

router.post('/', function (req, res, next) {
    User.create({
        username: req.body.username,
        password: md5(req.body.password)
    }).then(contact => {
        res.status(202).send(contact);
    }).catch(err => {
        res.status(400).send(err);
    });
});

router.use('*', (req, res) => {
    res.status(405).send('405 Method Not Allowed');
});

module.exports = router;
