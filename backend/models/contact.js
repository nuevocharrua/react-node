const { DataTypes, Model } = require('sequelize');
const db = require('../database');

class Contact extends Model { }

Contact.init({
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    userId: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
}, {
    sequelize: db,
    modelName: 'Contact'
});

module.exports = Contact;