import { ADD_CONTACT, REMOVE_CONTACT, FETCH_CONTACT, EMPTY_CONTACT } from "../actionTypes";

const initialState = {
    fetchAll: false,
    contacts: []
};

export default function contact(state = initialState, action) {
    switch (action.type) {
        case ADD_CONTACT: {
            const contact = action.payload;
            return {
                ...state,
                contacts: [...state.contacts, contact]
            };
        };
        case REMOVE_CONTACT: {
            const index = action.payload;
            return {
                ...state,
                contacts: [
                    ...state.contacts.slice(0, index),
                    ...state.contacts.slice(index + 1)
                ]
            };
        };

        case FETCH_CONTACT: {
            const contacts = action.payload;
            return {
                contacts: [...contacts],
                fetchAll: true
            };
        };
        case EMPTY_CONTACT:
            return {
                contacts: [],
                fetchAll: false
            };

        default:
            return state;
    }
}
