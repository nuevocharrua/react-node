import { ADD_CONTACT, REMOVE_CONTACT, FETCH_CONTACT, EMPTY_CONTACT } from "./actionTypes";

export const addContact = contact => ({
    type: ADD_CONTACT,
    payload: contact
});

export const removeContact = index => ({
    type: REMOVE_CONTACT,
    payload: index
});

export const fetchContact = contacts => ({
    type: FETCH_CONTACT,
    payload: contacts
});

export const emptyContact = () => ({
    type: EMPTY_CONTACT
});
