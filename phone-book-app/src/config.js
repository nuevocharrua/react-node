const configDev = {
    BASE_URL: 'http://localhost:3000'
}
const configProd = {
    BASE_URL: window.location.origin
}


export const config = process.env.NODE_ENV === 'development' ? configDev : configProd;