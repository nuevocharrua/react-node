import './App.css';
import React from "react";
import { Contacts } from './containers/Contacts'
import { ContactDetails } from './containers/ContactDetails'
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { Login } from './components/Login';
import { Header } from './containers/Header'
import { ProvideAuth } from './components/ProvideAuth'
import { PrivateRoute } from './components/PrivateRoute'
import { RegisterUser } from './components/RegisterUser';
function App() {

  return (
    <ProvideAuth>
      <Header></Header>
      <div className='main' >
        <Router>
          <Switch>
            <PrivateRoute path="/contacts">
              <Contacts></Contacts>
            </PrivateRoute>
            <PrivateRoute path="/details/:id">
              <ContactDetails></ContactDetails>
            </PrivateRoute>
            <Route path="/login">
              <Login></Login>
            </Route>
            <Route path="/register">
              <RegisterUser></RegisterUser>
            </Route>
            <Route exact path="/">
              <Redirect to='/contacts'></Redirect>
            </Route>
            <Route path="/404" >
              404 Page Not Found
            </Route>
            <Redirect to="/404" />
          </Switch>
        </Router>
      </div>
    </ProvideAuth>
  );
}

export default App;


