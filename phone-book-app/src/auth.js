import { useContext, createContext, useState } from "react";
import { Api } from './api';
export const authContext = createContext();
export const useAuth = () => {
    return useContext(authContext);
}

export const useProvideAuth = () => {
    const savedUser = JSON.parse(localStorage.getItem('user'));
    const [user, setUser] = useState(savedUser);

    const signin = (signinUser, ok, error) => {
        return auth.signin(signinUser, (token) => {
            localStorage.setItem('token', token.data);
            localStorage.setItem('user', JSON.stringify({ username: signinUser.username }));
            setUser(signinUser);
            ok();
        }, error);
    };

    const signout = ok => {
        return auth.signout(() => {
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            localStorage.clear();
            setUser(null);
            ok();
        });
    };

    return {
        user,
        signin,
        signout
    };
}


const auth = {
    isAuthenticated: false,
    signin(user, ok, error) {
        Api.login(user).then(res => {
            auth.isAuthenticated = true;
            ok(res);
        }).catch(err => {
            error(err);
        });
    },
    signout(cb) {
        auth.isAuthenticated = false;
        cb();
    }
};