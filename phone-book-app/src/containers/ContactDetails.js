import React, { useEffect, useState } from 'react'
import { useParams, Link } from "react-router-dom";
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Divider from '@material-ui/core/Divider';
import { useDispatch, useSelector } from "react-redux";
import { addContact } from '../redux/actions'
import { Api } from '../api';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));
export const ContactDetails = () => {
    const classes = useStyles();
    const { id } = useParams();
    const dispatch = useDispatch();
    const [error, setError] = useState(false)
    const contact = useSelector(state => state.contacts.contacts).filter(contact => contact.id === +id)[0];
    useEffect(() => {
        if (!contact) {
            Api.contact.get(id)
                .then((result) => result.data)
                .then(contact => {
                    setError(false);
                    dispatch(addContact(contact));
                }).catch(() => {
                    setError(true);
                });
        }
    }, []);
    return (
        <>
            <Link to='/contacts' >
                <Button edge="end" aria-label="comments"> Go Back </Button>
            </Link>
            {contact ? <List className={classes.root}>
                <ListItem>
                    <ListItemText primary={contact.firstName} secondary="First Name" />
                </ListItem>
                <Divider component="li" />
                <ListItem>
                    <ListItemText primary={contact.lastName} secondary="Last Name" />
                </ListItem>
                <Divider component="li" />
                <ListItem>
                    <ListItemText primary={contact.phone} secondary="Phone" />
                </ListItem>
            </List> : (<>{error && <SnackbarContent message='An error has occurre' />}</>)}
        </>
    )
}