import React, { useEffect } from 'react'
import { AddContact } from '../components/AddContact';
import { ContactsList } from '../components/ContactsList';
import { removeContact, addContact, fetchContact } from '../redux/actions'
import { useDispatch, useSelector } from "react-redux";
import { Api } from '../api';
import { Title } from '../components/Title'
export const Contacts = () => {
    const contactsList = useSelector(state => state.contacts);
    const contacts = contactsList.contacts
    const dispatch = useDispatch();

    useEffect(() => {
        async function fetchData() {
            if (!contactsList.fetchAll) {
                var result = await (await Api.contact.all()).data;
                dispatch(fetchContact(result));
            }
        }
        fetchData();
    }, []);

    const add = async (contact) => {
        var result = await (await Api.contact.add(contact)).data;
        contact.id = result.id;
        dispatch(addContact(contact));
    }
    const remove = (index) => {
        Api.contact.delete(contacts[index].id);
        dispatch(removeContact(index));
    }
    return (
        <>
            <Title text="Contacts"></Title>
            <AddContact onAdd={(contact) => add(contact)} ></AddContact>
            <ContactsList contacts={contacts} onRemove={(index) => remove(index)} ></ContactsList>
        </>
    )
}

