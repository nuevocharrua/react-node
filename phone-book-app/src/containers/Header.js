import React from 'react'
import { useAuth } from '../auth'
import { Button } from '@material-ui/core'
import { useDispatch } from "react-redux";
import { emptyContact } from '../redux/actions'


export const Header = () => {
    const auth = useAuth();
    const dispatch = useDispatch();
    const handlerSignOut = () => {
        auth.signout(() => dispatch(emptyContact()));
    }
    return (
        <div className='flex-end' >
            {auth.user &&
                <div>
                    Welcome {auth.user.username}
                    <Button color="primary" onClick={handlerSignOut} >LogOut</Button>
                </div>
            }
        </div>
    )
}


