import axios from 'axios';
import { config } from './config';

export const Api = {
    login: (user) => axios.post(`${config.BASE_URL}/api/login`, user),
    contact: {
        all: () => axios.get(`${config.BASE_URL}/api/contacts`, getHeader()),
        get: (id) => axios.get(`${config.BASE_URL}/api/contacts/${id}`, getHeader()),
        add: (contact) => axios.post(`${config.BASE_URL}/api/contacts`, contact, getHeader()),
        delete: (id) => axios.delete(`${config.BASE_URL}/api/contacts/${id}`, getHeader())
    },
    user: {
        add: (user) => axios.post(`${config.BASE_URL}/api/user`, user),
    }
}

const getToken = () => localStorage.getItem('token');
const getHeader = () => { return { headers: { "Authorization": `Bearer ${getToken()}` } } };