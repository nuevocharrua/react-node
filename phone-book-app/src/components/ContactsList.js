import React from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
export const ContactsList = ({ contacts, onRemove }) => {

    return (
        <List>
            {contacts.map((contact, index) => <ListItem key={index}>
                <Link to={`/details/${contact.id}`} >
                    <Button edge="end" aria-label="comments"> View </Button>
                </Link>
                <ListItemText primary={`${contact.firstName}, ${contact.phone}`} />
                <ListItemSecondaryAction>
                    <Button onClick={() => onRemove(index)} edge="end" aria-label="comments"> X </Button>
                </ListItemSecondaryAction>
            </ListItem>)}
        </List>
    )
}