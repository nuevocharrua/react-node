import React from 'react'

export const Title = ({ text }) => {
    return (
        <h2 className='header-title'  >{text}</h2>
    )
}
