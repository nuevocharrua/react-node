import { Button, TextField } from '@material-ui/core'
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { useHistory, useLocation, Link } from "react-router-dom";
import { Title } from './Title'
import React, { useState } from 'react'
import { useAuth } from '../auth'


export const Login = () => {
    const history = useHistory();
    const location = useLocation();
    const auth = useAuth();
    const [user, setUser] = useState({ username: '', password: '' });
    const [error, setError] = useState(false)
    const { from } = location.state || { from: { pathname: "/" } };
    const login = (e) => {
        e.preventDefault();
        auth.signin({ username: user.username, password: user.password }, () => {
            setError(false);
            history.replace(from);
        }, () => {
            setError(true);
        });
    };

    return (
        <div>
            <Title text="Login"></Title>
            <form noValidate autoComplete="off" onSubmit={e => login(e)} >
                <div>
                    <TextField className='add-input' label="UserName" onChange={e => setUser({ ...user, username: e.target.value.trim().toLowerCase() })} value={user.username} variant="outlined" />
                    <TextField className='add-input' type='password' label="Password" onChange={e => setUser({ ...user, password: e.target.value })} value={user.password} variant="outlined" />
                </div>
                <div className='flex-between' >
                    <Link className='register-link' to='/register'>Register New User</Link>
                    <Button variant="contained" disabled={!user.username || !user.password} type="submit" color="primary" >Sign In</Button>
                </div>

            </form>
            {error && <> <br /> <SnackbarContent message='UserName or Password invalid.' /> </>}
        </div>
    )
}


