import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useState } from 'react'


export const AddContact = ({ onAdd }) => {
    const [contact, setContact] = useState({ firstName: '', lastName: '', phone: '', id: 0 });
    const [show, setShow] = useState(false);
    const handleAddContact = () => {
        onAdd(contact);
        setContact({ firstName: '', lastName: '', phone: '', id: 0 });
    };
    return (
        <div>
            <div className='flex-end' >
                <Button color={show ? 'secondary' : 'primary'} onClick={() => { setShow(!show) }}  >
                    {show ? 'Close' : 'Add Contact'}
                </Button>
            </div>
            { show &&
                <form noValidate autoComplete="off">
                    <div>
                        <TextField className='add-input' label="First Name" onChange={e => setContact({ ...contact, firstName: e.target.value })} value={contact.firstName} variant="outlined" />
                        <TextField className='add-input' label="Last Name" onChange={e => setContact({ ...contact, lastName: e.target.value })} value={contact.lastName} variant="outlined" />
                        <TextField className='add-input' label="Phone" onChange={e => setContact({ ...contact, phone: e.target.value })} value={contact.phone} variant="outlined" />
                    </div>
                    <div className='flex-end' >
                        <Button type="submit" disabled={!contact.firstName.trim() || !contact.phone.trim()} variant="contained" color="primary" onClick={handleAddContact}>
                            Add
                        </Button>
                    </div>
                </form>
            }
        </div>
    )
}
