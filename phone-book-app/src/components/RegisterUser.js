import React, { useState } from 'react'
import { Button, TextField } from '@material-ui/core'
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { useHistory, Link } from "react-router-dom";
import { Api } from '../api'
import { Title } from './Title'

export const RegisterUser = () => {
    const history = useHistory();
    const [user, setUser] = useState({ username: '', password: '' });
    const [error, setError] = useState(false)
    const [confirmPassword, setConfirmPassword] = useState('')
    const register = (e) => {
        setError(false);
        e.preventDefault();
        Api.user.add(user).then(() => {
            history.push('/login');
        }).catch(() => {
            setError(true);
        });
        setUser({ username: '', password: '' });
        setConfirmPassword('');
    };
    return (
        <div>
            <Title text="New User"></Title>
            <form noValidate autoComplete="off" onSubmit={e => register(e)} >
                <div>
                    <TextField className='add-input' label="UserName" onChange={e => setUser({ ...user, username: e.target.value.trim().toLowerCase() })} value={user.username} variant="outlined" />
                    <TextField className='add-input' type='password' label="Password" onChange={e => setUser({ ...user, password: e.target.value })} value={user.password} variant="outlined" />
                    <TextField className='add-input' type='password' label="Confirm Password" onChange={e => setConfirmPassword(e.target.value)} value={confirmPassword} variant="outlined" />
                    {(user.password !== confirmPassword) && <SnackbarContent className='m10' message='Password are not matching' />}
                </div>
                <div className='flex-between' >
                    <Link className='register-link' to='/login'>Back</Link>
                    <Button variant="contained" disabled={!user.username || !user.password || (user.password !== confirmPassword)} type="submit" color="primary" >Create</Button>
                </div>
            </form>
            { error && <SnackbarContent className='m10' message='An error has occurre' />}
        </div>
    )
}
